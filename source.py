import pygame, sys

#General Setup 
pygame.init()  
clock = pygame.time.Clock() #to init frame counter - for cycles

# Setting up the main window
# defining window demensions 
screen_width  = 1280
screen_height = 960
screen = pygame.display.set_mode((screen_width,screen_height))
pygame.display.set_caption("Nathan.t - Pong")

#Game Rectangles -> to be displayed on top of main screen defied
#Imagine like a stack
# method Rect(x,y,width,height
# By default Rect(begin with (0,0))

#define game variables
tile_size = 160

#Defining Color -> 2 methods available as below
bg_color = pygame.Color('grey12')
light_grey = (200,200,200)

#adding visuals
#setting the speed at 7 for now
ball_speed_x = 7
ball_speed_y = 7

#adding grid patterns -> guidance for visuals
# def drawGrid():
#     for line in range(0,9):
#         pygame.draw.line(screen, light_grey, (screen_width/2-640, line* tile_size),(screen_width, line*tile_size))
#         pygame.draw.line(screen, light_grey, (line*tile_size,0),(line*tile_size, screen_height))

#Rect
ball = pygame.Rect(screen_width/2-15, screen_height/2-15,30,30)
player = pygame.Rect(screen_width-20, screen_height/2-70,10,140) 
opponent = pygame.Rect(10, screen_height/2 - 70,10,140)

start = True
while start:
    #Handling user input
    # drawGrid()
    for event in pygame.event.get():
         if event.type == pygame.QUIT:
             pygame.quit()
             sys.exit() 


    #executed ball movement
    ball.x += ball_speed_x
    ball.y += ball_speed_y


    #Drawing -> Visuals
    #pygame.draw(surface,color,rect)
    #takes in (screen, color, rect) 

    screen.fill(bg_color)
    pygame.draw.rect(screen,light_grey,player)
    pygame.draw.rect(screen,light_grey, opponent)
    pygame.draw.ellipse(screen, light_grey,ball)

    #method below takes 4 args(surface, color, tuple(start point,end poin))
    #anti aliance line 
    pygame.draw.aaline(screen, light_grey, (screen_width/2,0), (screen_width/2, screen_height))


    
    #updating the window
    pygame.display.flip()
    clock.tick(60)

