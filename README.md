# Ponng

Simple Pong Game written in python

## Installation

```bash
pip install pygame
```

## Usage

```python
import pygame

```

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to submit any bugs encounter

## License
[MIT](https://choosealicense.com/licenses/mit/)
